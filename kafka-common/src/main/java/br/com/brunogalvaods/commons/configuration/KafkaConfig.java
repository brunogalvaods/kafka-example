package br.com.brunogalvaods.commons.configuration;

import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;

public class KafkaConfig {

  /** Inject the kafka SSL config. */
  @Autowired private KafkaSslConfig kafkaSslConfig;

  protected void setKafkaSslLocation(
      final Map<String, Object> props, final String secretNameSuffix) {
    Optional.ofNullable(String.valueOf(props.get("ssl.keystore.location")))
        .ifPresent(
            location -> {
              kafkaSslConfig.setKeyStoreLocation(location, secretNameSuffix);
              props.put("ssl.keystore.location", location);
            });
  }
}
