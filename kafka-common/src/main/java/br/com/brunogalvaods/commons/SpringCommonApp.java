package br.com.brunogalvaods.commons;

import java.nio.charset.Charset;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

/** Spring boot generic application class. */
@SpringBootApplication(
    scanBasePackages = "br.com.brunogalvaods.*",
    exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public abstract class SpringCommonApp {

  /** Class logger instance. */
  private static final Logger LOGGER = LoggerFactory.getLogger(SpringCommonApp.class);

  /**
   * Runs the generic application.
   *
   * @param appClass Application configuration class.
   * @param args Initialization arguments.
   */
  public static void run(final Class<?> appClass, final String[] args) {
    LOGGER.info("Locale is: '{}'. Charset is '{}'.", Locale.getDefault(), Charset.defaultCharset());

    final SpringApplication springApplication = new SpringApplication(appClass);
    springApplication.run(args);
  }

  /**
   * Runs the generic application.
   *
   * @param args Initialization arguments.
   */
  public static void main(final String[] args) {
    SpringCommonApp.run(SpringCommonApp.class, args);
  }
}
