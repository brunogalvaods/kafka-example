package br.com.brunogalvaods.commons.properties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "rebel.kafka.tech")
public class KafkaTopicTechProps {

  private final Map<String, Object> properties = new HashMap();

  private List<Topic> topics = new ArrayList<>();

  public List<Topic> getTopics() {
    return topics;
  }

  public void setTopics(List<Topic> topics) {
    this.topics = topics;
  }

  public KafkaTopicTechProps() {}

  public List<Topic> buildTopicProperties() {
    return this.topics;
  }

  public static class Topic {

    private String name;
    private Integer numPartitions;
    private Short replicationFactor;

    private final Map<String, String> configs = new HashMap();

    public Topic() {}

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public Integer getNumPartitions() {
      return numPartitions;
    }

    public void setNumPartitions(Integer numPartitions) {
      this.numPartitions = numPartitions;
    }

    public Short getReplicationFactor() {
      return replicationFactor;
    }

    public void setReplicationFactor(Short replicationFactor) {
      this.replicationFactor = replicationFactor;
    }

    public Map<String, String> getConfigs() {
      return this.configs;
    }
  }

  private static class Properties extends HashMap<String, Object> {
    private Properties() {}

    public <V> java.util.function.Consumer<V> in(String key) {
      return (value) -> {
        this.put(key, value);
      };
    }

    public KafkaTopicTechProps.Properties with(Map<String, String> properties) {
      this.putAll(properties);
      return this;
    }
  }
}
