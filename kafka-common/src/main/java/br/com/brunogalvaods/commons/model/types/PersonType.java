package br.com.brunogalvaods.commons.model.types;

public enum PersonType {
  INDIVIDUAL(0L),
  JURIDICAL(1L);

  private Long id;

  PersonType(final Long id) {
    this.setId(id);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
