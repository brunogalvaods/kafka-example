package br.com.brunogalvaods.commons.model;

import br.com.brunogalvaods.commons.model.types.PersonType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KafkaModel {
  private String firstName;
  private String lastName;
  private PersonType personType;
}
