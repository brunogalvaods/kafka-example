package br.com.brunogalvaods.commons.properties;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.kafka.security.jaas.KafkaJaasLoginModuleInitializer.ControlFlag;
import org.springframework.util.CollectionUtils;

@Configuration
@ConfigurationProperties(prefix = "spring.kafka")
public class KafkaProps {

  private List<String> bootstrapServers =
      new ArrayList<>(Collections.singletonList("localhost:9092"));
  private String clientId;
  private final Map<String, String> properties = new HashMap<>();
  private final KafkaProps.Consumer consumer = new KafkaProps.Consumer();
  private final KafkaProps.Producer producer = new KafkaProps.Producer();
  private final KafkaProps.Admin admin = new KafkaProps.Admin();
  private final KafkaProps.Streams streams = new KafkaProps.Streams();
  private final KafkaProps.Listener listener = new KafkaProps.Listener();
  private final KafkaProps.Ssl ssl = new KafkaProps.Ssl();
  private final KafkaProps.Jaas jaas = new KafkaProps.Jaas();
  private final KafkaProps.Template template = new KafkaProps.Template();

  public KafkaProps() {}

  public List<String> getBootstrapServers() {
    return this.bootstrapServers;
  }

  public void setBootstrapServers(List<String> bootstrapServers) {
    this.bootstrapServers = bootstrapServers;
  }

  public String getClientId() {
    return this.clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public Map<String, String> getProperties() {
    return this.properties;
  }

  public KafkaProps.Consumer getConsumer() {
    return this.consumer;
  }

  public KafkaProps.Producer getProducer() {
    return this.producer;
  }

  public KafkaProps.Listener getListener() {
    return this.listener;
  }

  public KafkaProps.Admin getAdmin() {
    return this.admin;
  }

  public KafkaProps.Streams getStreams() {
    return this.streams;
  }

  public KafkaProps.Ssl getSsl() {
    return this.ssl;
  }

  public KafkaProps.Jaas getJaas() {
    return this.jaas;
  }

  public KafkaProps.Template getTemplate() {
    return this.template;
  }

  private Map<String, Object> buildCommonProperties() {
    Map<String, Object> properties = new HashMap<>();
    if (this.bootstrapServers != null) {
      properties.put("bootstrap.servers", this.bootstrapServers);
    }

    if (this.clientId != null) {
      properties.put("client.id", this.clientId);
    }

    properties.putAll(this.ssl.buildProperties());
    if (!CollectionUtils.isEmpty(this.properties)) {
      properties.putAll(this.properties);
    }

    return properties;
  }

  public Map<String, Object> buildConsumerProperties() {
    Map<String, Object> properties = this.buildCommonProperties();
    properties.putAll(this.consumer.buildProperties());
    return properties;
  }

  public Map<String, Object> buildProducerProperties() {
    Map<String, Object> properties = this.buildCommonProperties();
    properties.putAll(this.producer.buildProperties());
    return properties;
  }

  public Map<String, Object> buildAdminProperties() {
    Map<String, Object> properties = this.buildCommonProperties();
    properties.putAll(this.admin.buildProperties());
    return properties;
  }

  public Map<String, Object> buildStreamsProperties() {
    Map<String, Object> properties = this.buildCommonProperties();
    properties.putAll(this.streams.buildProperties());
    return properties;
  }

  private static class Properties extends HashMap<String, Object> {
    private Properties() {}

    public <V> java.util.function.Consumer<V> in(String key) {
      return (value) -> {
        this.put(key, value);
      };
    }

    public KafkaProps.Properties with(
        KafkaProps.Ssl ssl, Map<String, String> properties) {
      this.putAll(ssl.buildProperties());
      this.putAll(properties);
      return this;
    }
  }

  public static class Jaas {
    private boolean enabled;
    private String loginModule = "com.sun.security.auth.module.Krb5LoginModule";
    private ControlFlag controlFlag;
    private final Map<String, String> options;

    public Jaas() {
      this.controlFlag = ControlFlag.REQUIRED;
      this.options = new HashMap();
    }

    public boolean isEnabled() {
      return this.enabled;
    }

    public void setEnabled(boolean enabled) {
      this.enabled = enabled;
    }

    public String getLoginModule() {
      return this.loginModule;
    }

    public void setLoginModule(String loginModule) {
      this.loginModule = loginModule;
    }

    public ControlFlag getControlFlag() {
      return this.controlFlag;
    }

    public void setControlFlag(ControlFlag controlFlag) {
      this.controlFlag = controlFlag;
    }

    public Map<String, String> getOptions() {
      return this.options;
    }

    public void setOptions(Map<String, String> options) {
      if (options != null) {
        this.options.putAll(options);
      }
    }
  }

  public static class Ssl {
    private String keyPassword;
    private String keyStoreLocation;
    private String keyStorePassword;
    private String keyStoreType;
    private String trustStoreLocation;
    private String trustStorePassword;
    private String trustStoreType;
    private String protocol;

    public Ssl() {}

    public String getKeyPassword() {
      return this.keyPassword;
    }

    public void setKeyPassword(String keyPassword) {
      this.keyPassword = keyPassword;
    }

    public String getKeyStoreLocation() {
      return this.keyStoreLocation;
    }

    public void setKeyStoreLocation(String keyStoreLocation) {
      this.keyStoreLocation = keyStoreLocation;
    }

    public String getKeyStorePassword() {
      return this.keyStorePassword;
    }

    public void setKeyStorePassword(String keyStorePassword) {
      this.keyStorePassword = keyStorePassword;
    }

    public String getKeyStoreType() {
      return this.keyStoreType;
    }

    public void setKeyStoreType(String keyStoreType) {
      this.keyStoreType = keyStoreType;
    }

    public String getTrustStoreLocation() {
      return this.trustStoreLocation;
    }

    public void setTrustStoreLocation(String trustStoreLocation) {
      this.trustStoreLocation = trustStoreLocation;
    }

    public String getTrustStorePassword() {
      return this.trustStorePassword;
    }

    public void setTrustStorePassword(String trustStorePassword) {
      this.trustStorePassword = trustStorePassword;
    }

    public String getTrustStoreType() {
      return this.trustStoreType;
    }

    public void setTrustStoreType(String trustStoreType) {
      this.trustStoreType = trustStoreType;
    }

    public String getProtocol() {
      return this.protocol;
    }

    public void setProtocol(String protocol) {
      this.protocol = protocol;
    }

    public Map<String, Object> buildProperties() {
      KafkaProps.Properties properties = new KafkaProps.Properties();
      Map<String, Object> map = new HashMap<>();
      Optional.ofNullable(this.keyPassword).ifPresent(property -> map.put("ssl.key.password", property));
      Optional.ofNullable(this.keyStoreLocation).ifPresent(property -> map.put("ssl.keystore.location", property));
      Optional.ofNullable(this.keyStorePassword).ifPresent(property -> map.put("ssl.keystore.password", property));
      Optional.ofNullable(this.keyStoreType).ifPresent(property -> map.put("ssl.keystore.type", property));
      Optional.ofNullable(this.trustStoreLocation).ifPresent(property -> map.put("ssl.truststore.location", property));
      Optional.ofNullable(this.trustStorePassword).ifPresent(property -> map.put("ssl.truststore.password", property));
      Optional.ofNullable(this.trustStoreType).ifPresent(property -> map.put("ssl.truststore.type", property));
      Optional.ofNullable(this.protocol).ifPresent(property -> map.put("ssl.protocol", property));
      properties.putAll(map);
      return properties;
    }

    private String resourceToPath(Resource resource) {
      try {
        return resource.getFile().getAbsolutePath();
      } catch (IOException var3) {
        throw new IllegalStateException(
            "Resource '" + resource + "' must be on a file system", var3);
      }
    }
  }

  public static class Listener {
    private KafkaProps.Listener.Type type;
    //    private AckMode ackMode;
    private String clientId;
    private Integer concurrency;
    private Duration pollTimeout;
    private Float noPollThreshold;
    private Integer ackCount;
    private Duration ackTime;
    private Duration idleEventInterval;
    //    @DurationUnit(ChronoUnit.SECONDS)
    private Duration monitorInterval;
    private Boolean logContainerConfig;

    public Listener() {
      this.type = KafkaProps.Listener.Type.SINGLE;
    }

    public KafkaProps.Listener.Type getType() {
      return this.type;
    }

    public void setType(KafkaProps.Listener.Type type) {
      this.type = type;
    }

    //    public AckMode getAckMode() {
    //      return this.ackMode;
    //    }
    //
    //    public void setAckMode(AckMode ackMode) {
    //      this.ackMode = ackMode;
    //    }

    public String getClientId() {
      return this.clientId;
    }

    public void setClientId(String clientId) {
      this.clientId = clientId;
    }

    public Integer getConcurrency() {
      return this.concurrency;
    }

    public void setConcurrency(Integer concurrency) {
      this.concurrency = concurrency;
    }

    public Duration getPollTimeout() {
      return this.pollTimeout;
    }

    public void setPollTimeout(Duration pollTimeout) {
      this.pollTimeout = pollTimeout;
    }

    public Float getNoPollThreshold() {
      return this.noPollThreshold;
    }

    public void setNoPollThreshold(Float noPollThreshold) {
      this.noPollThreshold = noPollThreshold;
    }

    public Integer getAckCount() {
      return this.ackCount;
    }

    public void setAckCount(Integer ackCount) {
      this.ackCount = ackCount;
    }

    public Duration getAckTime() {
      return this.ackTime;
    }

    public void setAckTime(Duration ackTime) {
      this.ackTime = ackTime;
    }

    public Duration getIdleEventInterval() {
      return this.idleEventInterval;
    }

    public void setIdleEventInterval(Duration idleEventInterval) {
      this.idleEventInterval = idleEventInterval;
    }

    public Duration getMonitorInterval() {
      return this.monitorInterval;
    }

    public void setMonitorInterval(Duration monitorInterval) {
      this.monitorInterval = monitorInterval;
    }

    public Boolean getLogContainerConfig() {
      return this.logContainerConfig;
    }

    public void setLogContainerConfig(Boolean logContainerConfig) {
      this.logContainerConfig = logContainerConfig;
    }

    public static enum Type {
      SINGLE,
      BATCH;

      private Type() {}
    }
  }

  public static class Template {
    private String defaultTopic;

    public Template() {}

    public String getDefaultTopic() {
      return this.defaultTopic;
    }

    public void setDefaultTopic(String defaultTopic) {
      this.defaultTopic = defaultTopic;
    }
  }

  public static class Streams {
    private final KafkaProps.Ssl ssl = new KafkaProps.Ssl();
    private String applicationId;
    private boolean autoStartup = true;
    private List<String> bootstrapServers;
    private Integer cacheMaxSizeBuffering;
    private String clientId;
    private Integer replicationFactor;
    private String stateDir;
    private final Map<String, String> properties = new HashMap();

    public Streams() {}

    public KafkaProps.Ssl getSsl() {
      return this.ssl;
    }

    public String getApplicationId() {
      return this.applicationId;
    }

    public void setApplicationId(String applicationId) {
      this.applicationId = applicationId;
    }

    public boolean isAutoStartup() {
      return this.autoStartup;
    }

    public void setAutoStartup(boolean autoStartup) {
      this.autoStartup = autoStartup;
    }

    public List<String> getBootstrapServers() {
      return this.bootstrapServers;
    }

    public void setBootstrapServers(List<String> bootstrapServers) {
      this.bootstrapServers = bootstrapServers;
    }

    public Integer getCacheMaxSizeBuffering() {
      return this.cacheMaxSizeBuffering;
    }

    public void setCacheMaxSizeBuffering(Integer cacheMaxSizeBuffering) {
      this.cacheMaxSizeBuffering = cacheMaxSizeBuffering;
    }

    public String getClientId() {
      return this.clientId;
    }

    public void setClientId(String clientId) {
      this.clientId = clientId;
    }

    public Integer getReplicationFactor() {
      return this.replicationFactor;
    }

    public void setReplicationFactor(Integer replicationFactor) {
      this.replicationFactor = replicationFactor;
    }

    public String getStateDir() {
      return this.stateDir;
    }

    public void setStateDir(String stateDir) {
      this.stateDir = stateDir;
    }

    public Map<String, String> getProperties() {
      return this.properties;
    }

    public Map<String, Object> buildProperties() {
      KafkaProps.Properties properties = new KafkaProps.Properties();
      Map<String, Object> map = new HashMap<>();
      Optional.ofNullable(this.applicationId)
          .ifPresent(property -> map.put("application.id", property));
      Optional.ofNullable(this.bootstrapServers)
          .ifPresent(property -> map.put("bootstrap.servers", property));
      Optional.ofNullable(this.cacheMaxSizeBuffering)
          .ifPresent(property -> map.put("cache.max.bytes.buffering", property));
      Optional.ofNullable(this.clientId).ifPresent(property -> map.put("client.id", property));
      Optional.ofNullable(this.replicationFactor)
          .ifPresent(property -> map.put("replication.factor", property));
      Optional.ofNullable(this.stateDir).ifPresent(property -> map.put("state.dir", property));
      properties.putAll(map);
      return properties.with(this.ssl, this.properties);
    }
  }

  public static class Admin {
    private final KafkaProps.Ssl ssl = new KafkaProps.Ssl();
    private String clientId;
    private final Map<String, String> properties = new HashMap();
    private boolean failFast;

    public Admin() {}

    public KafkaProps.Ssl getSsl() {
      return this.ssl;
    }

    public String getClientId() {
      return this.clientId;
    }

    public void setClientId(String clientId) {
      this.clientId = clientId;
    }

    public boolean isFailFast() {
      return this.failFast;
    }

    public void setFailFast(boolean failFast) {
      this.failFast = failFast;
    }

    public Map<String, String> getProperties() {
      return this.properties;
    }

    public Map<String, Object> buildProperties() {
      KafkaProps.Properties properties = new KafkaProps.Properties();
      Map<String, Object> map = new HashMap<>();
      Optional.ofNullable(this.clientId).ifPresent(property -> map.put("client.id", property));
      properties.putAll(map);
      return properties.with(this.ssl, this.properties);
    }
  }

  public static class Producer {
    private final KafkaProps.Ssl ssl = new KafkaProps.Ssl();
    private String acks;
    private Integer batchSize;
    private List<String> bootstrapServers;
    private Integer bufferMemory;
    private String clientId;
    private String compressionType;
    private Class<?> keySerializer = StringSerializer.class;
    private Class<?> valueSerializer = StringSerializer.class;
    private Integer retries;
    private String transactionIdPrefix;
    private final Map<String, String> properties = new HashMap();

    public Producer() {}

    public KafkaProps.Ssl getSsl() {
      return this.ssl;
    }

    public String getAcks() {
      return this.acks;
    }

    public void setAcks(String acks) {
      this.acks = acks;
    }

    public Integer getBatchSize() {
      return this.batchSize;
    }

    public void setBatchSize(Integer batchSize) {
      this.batchSize = batchSize;
    }

    public List<String> getBootstrapServers() {
      return this.bootstrapServers;
    }

    public void setBootstrapServers(List<String> bootstrapServers) {
      this.bootstrapServers = bootstrapServers;
    }

    public Integer getBufferMemory() {
      return this.bufferMemory;
    }

    public void setBufferMemory(Integer bufferMemory) {
      this.bufferMemory = bufferMemory;
    }

    public String getClientId() {
      return this.clientId;
    }

    public void setClientId(String clientId) {
      this.clientId = clientId;
    }

    public String getCompressionType() {
      return this.compressionType;
    }

    public void setCompressionType(String compressionType) {
      this.compressionType = compressionType;
    }

    public Class<?> getKeySerializer() {
      return this.keySerializer;
    }

    public void setKeySerializer(Class<?> keySerializer) {
      this.keySerializer = keySerializer;
    }

    public Class<?> getValueSerializer() {
      return this.valueSerializer;
    }

    public void setValueSerializer(Class<?> valueSerializer) {
      this.valueSerializer = valueSerializer;
    }

    public Integer getRetries() {
      return this.retries;
    }

    public void setRetries(Integer retries) {
      this.retries = retries;
    }

    public String getTransactionIdPrefix() {
      return this.transactionIdPrefix;
    }

    public void setTransactionIdPrefix(String transactionIdPrefix) {
      this.transactionIdPrefix = transactionIdPrefix;
    }

    public Map<String, String> getProperties() {
      return this.properties;
    }

    public Map<String, Object> buildProperties() {
      KafkaProps.Properties properties = new KafkaProps.Properties();
      Map<String, Object> map = new HashMap<>();
      Optional.ofNullable(this.acks).ifPresent(property -> map.put("acks", property));
      Optional.ofNullable(this.batchSize).ifPresent(property -> map.put("batch.size", property));
      Optional.ofNullable(this.bootstrapServers)
          .ifPresent(property -> map.put("bootstrap.servers", property));
      Optional.ofNullable(this.bufferMemory)
          .ifPresent(property -> map.put("buffer.memory", property));
      Optional.ofNullable(this.clientId).ifPresent(property -> map.put("client.id", property));
      Optional.ofNullable(this.compressionType)
          .ifPresent(property -> map.put("compression.type", property));
      Optional.ofNullable(this.keySerializer)
          .ifPresent(property -> map.put("key.serializer", property));
      Optional.ofNullable(this.retries).ifPresent(property -> map.put("retries", property));
      Optional.ofNullable(this.valueSerializer)
          .ifPresent(property -> map.put("value.serializer", property));
      properties.putAll(map);
      return properties.with(this.ssl, this.properties);
    }
  }

  public static class Consumer {
    private final KafkaProps.Ssl ssl = new KafkaProps.Ssl();
    private Duration autoCommitInterval;
    private String autoOffsetReset;
    private List<String> bootstrapServers;
    private String clientId;
    private Boolean enableAutoCommit;
    private Duration fetchMaxWait;
    private Integer fetchMinSize;
    private String groupId;
    private Duration heartbeatInterval;
    private Class<?> keyDeserializer = StringDeserializer.class;
    private Class<?> valueDeserializer = StringDeserializer.class;
    private Integer maxPollRecords;
    private final Map<String, String> properties = new HashMap();

    public Consumer() {}

    public KafkaProps.Ssl getSsl() {
      return this.ssl;
    }

    public Duration getAutoCommitInterval() {
      return this.autoCommitInterval;
    }

    public void setAutoCommitInterval(Duration autoCommitInterval) {
      this.autoCommitInterval = autoCommitInterval;
    }

    public String getAutoOffsetReset() {
      return this.autoOffsetReset;
    }

    public void setAutoOffsetReset(String autoOffsetReset) {
      this.autoOffsetReset = autoOffsetReset;
    }

    public List<String> getBootstrapServers() {
      return this.bootstrapServers;
    }

    public void setBootstrapServers(List<String> bootstrapServers) {
      this.bootstrapServers = bootstrapServers;
    }

    public String getClientId() {
      return this.clientId;
    }

    public void setClientId(String clientId) {
      this.clientId = clientId;
    }

    public Boolean getEnableAutoCommit() {
      return this.enableAutoCommit;
    }

    public void setEnableAutoCommit(Boolean enableAutoCommit) {
      this.enableAutoCommit = enableAutoCommit;
    }

    public Duration getFetchMaxWait() {
      return this.fetchMaxWait;
    }

    public void setFetchMaxWait(Duration fetchMaxWait) {
      this.fetchMaxWait = fetchMaxWait;
    }

    public Integer getFetchMinSize() {
      return this.fetchMinSize;
    }

    public void setFetchMinSize(Integer fetchMinSize) {
      this.fetchMinSize = fetchMinSize;
    }

    public String getGroupId() {
      return this.groupId;
    }

    public void setGroupId(String groupId) {
      this.groupId = groupId;
    }

    public Duration getHeartbeatInterval() {
      return this.heartbeatInterval;
    }

    public void setHeartbeatInterval(Duration heartbeatInterval) {
      this.heartbeatInterval = heartbeatInterval;
    }

    public Class<?> getKeyDeserializer() {
      return this.keyDeserializer;
    }

    public void setKeyDeserializer(Class<?> keyDeserializer) {
      this.keyDeserializer = keyDeserializer;
    }

    public Class<?> getValueDeserializer() {
      return this.valueDeserializer;
    }

    public void setValueDeserializer(Class<?> valueDeserializer) {
      this.valueDeserializer = valueDeserializer;
    }

    public Integer getMaxPollRecords() {
      return this.maxPollRecords;
    }

    public void setMaxPollRecords(Integer maxPollRecords) {
      this.maxPollRecords = maxPollRecords;
    }

    public Map<String, String> getProperties() {
      return this.properties;
    }

    public Map<String, Object> buildProperties() {
      KafkaProps.Properties properties = new KafkaProps.Properties();
      Map<String, Object> map = new HashMap<>();
      Optional.ofNullable(this.autoCommitInterval)
          .ifPresent(property -> map.put("auto.commit.interval.ms", property));
      Optional.ofNullable(this.autoOffsetReset)
          .ifPresent(property -> map.put("auto.offset.reset", property));
      Optional.ofNullable(this.bootstrapServers)
          .ifPresent(property -> map.put("bootstrap.servers", property));
      Optional.ofNullable(this.clientId).ifPresent(property -> map.put("client.id", property));
      Optional.ofNullable(this.enableAutoCommit)
          .ifPresent(property -> map.put("enable.auto.commit", property));
      Optional.ofNullable(this.fetchMaxWait)
          .ifPresent(property -> map.put("fetch.max.wait.ms", property));
      Optional.ofNullable(this.fetchMinSize)
          .ifPresent(property -> map.put("fetch.min.bytes", property));
      Optional.ofNullable(this.groupId).ifPresent(property -> map.put("group.id", property));
      Optional.ofNullable(this.heartbeatInterval)
          .ifPresent(property -> map.put("heartbeat.interval.ms", property));
      Optional.ofNullable(this.keyDeserializer)
          .ifPresent(property -> map.put("key.deserializer", property));
      Optional.ofNullable(this.valueDeserializer)
          .ifPresent(property -> map.put("value.deserializer", property));
      Optional.ofNullable(this.maxPollRecords)
          .ifPresent(property -> map.put("max.poll.records", property));
      properties.putAll(map);
      return properties.with(this.ssl, this.properties);
    }
  }
}
