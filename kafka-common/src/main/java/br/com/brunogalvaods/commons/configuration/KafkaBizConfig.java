package br.com.brunogalvaods.commons.configuration;

import br.com.brunogalvaods.commons.exceptions.BusinessException;
import br.com.brunogalvaods.commons.exceptions.IntegrationException;
import br.com.brunogalvaods.commons.properties.KafkaProps;
import br.com.brunogalvaods.commons.properties.KafkaTopicBizProps;
import br.com.brunogalvaods.commons.properties.KafkaTopicBizProps.Topic;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import javax.annotation.PostConstruct;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.errors.TopicExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.AbstractMessageListenerContainer.AckMode;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;

/** This class is responsible for load and build the kafka producer and consumer configs. */
@Component("rebelKafkaBizConfig")
@ConditionalOnClass(KafkaTemplate.class)
@PropertySource(
    value = {
      "classpath:kafka-biz.properties",
    },
    ignoreResourceNotFound = true,
    encoding = "UTF-8")
public class KafkaBizConfig extends KafkaConfig {

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaBizConfig.class);

  /** Inject the kafka properties. */
  @Autowired private KafkaProps kafkaProperties;

  /** Inject the kafka topics properties. */
  @Autowired private KafkaTopicBizProps kafkaTopicBizProps;

  private KafkaTemplate<String, String> kafkaTemplate;

  private static final String SECRET_NAME_SUFFIX = "biz";

  @PostConstruct
  public void init() {
    this.kafkaTemplate = kafkaTemplate();
  }

  /**
   * Create a producer factory.
   *
   * @return a producer factory.
   */
  @Bean("producerFactoryBiz")
  public ProducerFactory<String, String> producerFactory() {
    Map<String, Object> props = kafkaProperties.buildProducerProperties();
    super.setKafkaSslLocation(props, SECRET_NAME_SUFFIX);
    return new DefaultKafkaProducerFactory<>(props);
  }

  /**
   * Create a Bean with the producer factory properties.
   *
   * @return a new <code>KafkaTemplate</code> instance.
   */
  @Bean("kafkaTemplateBiz")
  public KafkaTemplate<String, String> kafkaTemplate() {
    return new KafkaTemplate<>(producerFactory());
  }

  @Bean("consumerFactoryBiz")
  public ConsumerFactory<String, String> consumerFactory() {
    Map<String, Object> props = kafkaProperties.buildConsumerProperties();
    super.setKafkaSslLocation(props, SECRET_NAME_SUFFIX);
    return new DefaultKafkaConsumerFactory<>(props);
  }

  @Bean("consumerFactoryRetryBiz")
  public ConsumerFactory<String, String> consumerFactoryRetry() {
    Map<String, Object> props = kafkaProperties.buildConsumerProperties();
    final String groupId = (String) props.get("group.id");
    Optional.ofNullable(groupId)
        .ifPresent(property -> props.put("group.id", property.concat(".RETRY")));
    super.setKafkaSslLocation(props, SECRET_NAME_SUFFIX);
    return new DefaultKafkaConsumerFactory<>(props);
  }

  @Bean("kafkaListenerContainerFactoryBiz")
  public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
    ConcurrentKafkaListenerContainerFactory<String, String> factory =
        new ConcurrentKafkaListenerContainerFactory<>();
    // Sets consumer factory.
    factory.setConsumerFactory(consumerFactory());
    // Enable manual immediate commit.
    factory.getContainerProperties().setAckMode(AckMode.MANUAL_IMMEDIATE);
    // Sets the concurrency threads to processing.
    factory.setConcurrency(3);

    return factory;
  }

  @Bean("kafkaListenerContainerFactoryBizRetry")
  public ConcurrentKafkaListenerContainerFactory<String, String>
      kafkaListenerContainerFactoryBizRetry() {
    ConcurrentKafkaListenerContainerFactory<String, String> factory =
        new ConcurrentKafkaListenerContainerFactory<>();
    // Sets consumer factory.
    factory.setConsumerFactory(consumerFactoryRetry());
    // Enable manual immediate commit.
    factory.getContainerProperties().setAckMode(AckMode.MANUAL_IMMEDIATE);
    // Sets the concurrency threads to processing.
    factory.setConcurrency(3);
    // Configures the listener container factory with retry support
    factory.setRetryTemplate(retryTemplate());
    // Sets the recovery callback after retries the quantity times defined.
    factory.setRecoveryCallback(
        retryContext -> {
          // Gets the consumer record.
          final ConsumerRecord<String, String> consumerRecord =
              (ConsumerRecord<String, String>) retryContext.getAttribute("record");
          // Gets the acknowledgment.
          final Acknowledgment ack = (Acknowledgment) retryContext.getAttribute("acknowledgment");

          LOGGER.info("thread id: {}", Thread.currentThread().getId());
          LOGGER.info("Recovery is called for message {} ", consumerRecord.value());

          if (retryContext.getLastThrowable().getCause() instanceof IntegrationException) {
            final Message<String> message =
                MessageBuilder.withPayload(consumerRecord.value())
                    .setHeader(KafkaHeaders.TOPIC, consumerRecord.topic().replace(".RETRY", ".DLT"))
                    .build();

            kafkaTemplate.send(message);
          }

          // Manual commit.
          ack.acknowledge();

          return Optional.empty();
        });

    return factory;
  }

  /**
   * Retryable operations are encapsulated in implementations of the {@link RetryCallback} interface
   * and are executed using one of the supplied execute methods.
   *
   * @return
   */
  @Bean("retryTemplateBiz")
  public RetryTemplate retryTemplate() {
    final RetryTemplate retryTemplate = new RetryTemplate();

    // Implementation of {@link BackOffPolicy} that increases the back off period
    // for each retry attempt in a given set using the {@link Math#exp(double)
    // exponential} function.This implementation is thread-safe and suitable for
    // concurrent access. Modifications to the configuration do not affect any retry
    // sets that are already in progress.
    final ExponentialBackOffPolicy exponentialBackOffPolicy = new ExponentialBackOffPolicy();

    // Set the multiplier value. Default is '<code>2.0</code>'. Hint: do not use
    // values much in excess of 1.0 (or the backoff will get very long very fast).
    exponentialBackOffPolicy.setMultiplier(2.0);
    // Set the initial sleep interval value. Default is {@code 100} millisecond.
    // Cannot be set to a value less than one.
    exponentialBackOffPolicy.setInitialInterval(1000);
    // Sets the backoff policy with the exponential backoff policy.
    retryTemplate.setBackOffPolicy(exponentialBackOffPolicy);

    // Create a {@link SimpleRetryPolicy} with the default number of retry attempts,
    // retrying all exceptions.
    final Map<Class<? extends Throwable>, Boolean> exceptionsMap = new HashMap<>();
    exceptionsMap.put(IntegrationException.class, true);
    exceptionsMap.put(BusinessException.class, false);
    // Sets the maximum number of attempts.
    final int maxAttempts = 3;
    // New instance SimpleRetryPolicy.
    final SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy(maxAttempts, exceptionsMap, true);
    // Sets the retry policy.
    retryTemplate.setRetryPolicy(retryPolicy);

    return retryTemplate;
  }

  /**
   * Create the kafka admin.
   *
   * @return a kafka admin.
   */
  @Bean("adminBiz")
  public KafkaAdmin admin() {
    Map<String, Object> props = kafkaProperties.buildAdminProperties();
    super.setKafkaSslLocation(props, SECRET_NAME_SUFFIX);
    return new KafkaAdmin(props);
  }

  /**
   * Create the topics.
   *
   * @param kafkaAdmin
   * @return
   */
  @Bean("createBizTopics")
  public ApplicationRunner createTopics(KafkaAdmin kafkaAdmin) {
    return args -> {
      // create admin client config.
      final AdminClient admin = AdminClient.create(kafkaAdmin.getConfig());
      // create the new topics list.
      final List<NewTopic> topics = new ArrayList<>();
      // get the topics properties.
      final List<Topic> topicsProperties = kafkaTopicBizProps.buildTopicProperties();
      // iterate the topics props.
      for (final Topic topicProp : topicsProperties) {
        // new instance topic.
        final NewTopic newTopic =
            new NewTopic(
                topicProp.getName(),
                topicProp.getNumPartitions(),
                topicProp.getReplicationFactor());
        // add the others configs.
        newTopic.configs(topicProp.getConfigs());
        // add the new topic in list.
        topics.add(newTopic);
      }
      // build the list.
      try {
        admin.createTopics(topics).all().get();
      } catch (final ExecutionException e) {
        if (e.getCause() instanceof TopicExistsException) {
          LOGGER.debug("createTopics() -> {}", e.getMessage());
        } else {
          throw e;
        }
      }
    };
  }
}
