package br.com.brunogalvaods.commons.configuration;

import br.com.brunogalvaods.commons.exceptions.IntegrationException;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.DecryptionFailureException;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.amazonaws.services.secretsmanager.model.InternalServiceErrorException;
import com.amazonaws.services.secretsmanager.model.InvalidParameterException;
import com.amazonaws.services.secretsmanager.model.InvalidRequestException;
import com.amazonaws.services.secretsmanager.model.ResourceNotFoundException;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class KafkaSslConfig {

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaSslConfig.class);

  @Value("${ENV}")
  String environment;

  private static final String AWS_REGION = "us-east-1";

  private static final String USER_HOME_DIRECTORY = System.getProperty("user.home");

  private static final String AWS_KAFKA_JKS_LOCATION = USER_HOME_DIRECTORY.concat("/services/secrets/kafka.aws.jks");

  public void setKeyStoreLocation(final String keyStoreLocation, final String secretSuffix) {
    final String secretName = this.environment.concat("-kafka-").concat(secretSuffix);
    getSecret(secretName, keyStoreLocation);
  }

  private void getSecret(final String secretName, final String location) {
    // Create a Secrets Manager client
    final AWSSecretsManager client =
        AWSSecretsManagerClientBuilder.standard().withRegion(AWS_REGION).build();

    final GetSecretValueRequest getSecretValueRequest =
        new GetSecretValueRequest().withSecretId(secretName);
    GetSecretValueResult getSecretValueResult;

    try {
      getSecretValueResult = client.getSecretValue(getSecretValueRequest);
    } catch (final DecryptionFailureException
        | InvalidParameterException
        | InvalidRequestException
        | InternalServiceErrorException
        | ResourceNotFoundException e) {
      throw new IntegrationException();
    }

    try {
      FileUtils.writeByteArrayToFile(
          new File(AWS_KAFKA_JKS_LOCATION), getSecretValueResult.getSecretBinary().array());

      FileUtils.writeByteArrayToFile(
          new File(location),
          Base64.getMimeDecoder()
              .decode(FileUtils.readFileToByteArray(new File(AWS_KAFKA_JKS_LOCATION))));
    } catch (final IOException e) {
      LOGGER.error(e.getMessage());
    }
  }
}
