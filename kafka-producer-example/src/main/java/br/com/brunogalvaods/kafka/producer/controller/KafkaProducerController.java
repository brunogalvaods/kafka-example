package br.com.brunogalvaods.kafka.producer.controller;

import br.com.brunogalvaods.commons.model.KafkaModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/producer")
public class KafkaProducerController {

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerController.class);

  @Autowired
  @Qualifier("kafkaTemplateTech")
  private KafkaTemplate<String, String> kafkaTemplate;

  /** Cache manager object. */
  @Autowired private CacheManager cacheManager;

  private static final String CACHE_NAME = "topic-cachename";

  private static final String CACHE_KEY = "topic-key";

  @Autowired private ObjectMapper objectMapper;

  @Value("${kafka.web.topic.name}")
  private String topicName;

  /**
   * @param model
   * @return
   * @throws JsonProcessingException
   */
  @PostMapping("/kafkaExampleTopic3")
  public ResponseEntity<?> postWeb(@RequestBody final KafkaModel model)
      throws JsonProcessingException {

    final String messageType = "WEB";

    final String json = objectMapper.writeValueAsString(model);

    final Message<String> message =
        MessageBuilder.withPayload(json)
            .setHeader(KafkaHeaders.TOPIC, topicName)
            .setHeader("x-message-type", messageType)
            .build();

    try {
      final ListenableFuture<SendResult<String, String>> listenableFuture =
          kafkaTemplate.send(message);
      listenableFuture.addCallback(
          new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onSuccess(SendResult<String, String> result) {
              LOGGER.info("Success sending an event to kafka");
            }

            @Override
            public void onFailure(Throwable ex) {
              LOGGER.error("Failed sending an event to kafka", ex);
            }
          });

      listenableFuture.get(1000, TimeUnit.MILLISECONDS);

      LOGGER.info(
          "Producer WEB - First name: {}, Last name: {}",
          model.getFirstName(),
          model.getLastName());
      return new ResponseEntity<>(model, HttpStatus.CREATED);
    } catch (final Exception e) {
      LOGGER.error("Failed sending an event to kafka.");
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * @param model
   * @return
   * @throws JsonProcessingException
   */
  @PostMapping("/myTopicMobile")
  public ResponseEntity<KafkaModel> postMobile(@RequestBody final KafkaModel model)
      throws JsonProcessingException {

    final String messageType = "MOBILE";

    final String json = objectMapper.writeValueAsString(model);

    final Message<String> message =
        MessageBuilder.withPayload(json)
            .setHeader(KafkaHeaders.TOPIC, "mobileTopic")
            .setHeader("x-message-type", messageType)
            .build();

    this.kafkaTemplate.send(message);

    LOGGER.info(
        "Producer MOBILE - First name: {}, Last name: {}",
        model.getFirstName(),
        model.getLastName());
    return new ResponseEntity<>(model, HttpStatus.CREATED);
  }

  /**
   * @param model
   * @return
   * @throws JsonProcessingException
   */
  @PostMapping("/myTopicAny")
  public ResponseEntity<KafkaModel> postAny(@RequestBody final KafkaModel model)
      throws JsonProcessingException {

    final String messageType = "ANY";

    final String json = objectMapper.writeValueAsString(model);

    // Build the message.
    final Message<String> message =
        MessageBuilder.withPayload(json)
            .setHeader(KafkaHeaders.TOPIC, getTopicSended())
            .setHeader("x-message-type", messageType)
            .build();

    this.kafkaTemplate.send(message);

    LOGGER.info(
        "Producer ANY - First name: {}, Last name: {}", model.getFirstName(), model.getLastName());
    return new ResponseEntity<>(model, HttpStatus.CREATED);
  }

  private synchronized String getTopicSended() {
    String topic = "webTopic";
    final Cache cache = this.cacheManager.getCache(CACHE_NAME);
    if (cache.get(CACHE_KEY) != null) {
      final String lastTopicSended = (String) cache.get(CACHE_KEY).get();
      if ("webTopic".equalsIgnoreCase(lastTopicSended)) {
        topic = "mobileTopic";
      }
    }

    // saves the last topic to sended.
    cache.put(CACHE_KEY, topic);

    return topic;
  }
}
