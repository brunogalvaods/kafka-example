#!/bin/bash

# Zip execution result.
zip /Users/bruno/_github-personal/kafka-example/kafka-producer-example/output_results.zip /Users/bruno/_github-personal/kafka-example/kafka-producer-example/tm4j_result.json

# Send execution result.
curl --location --request POST 'https://api.adaptavist.io/tm4j/v2/automations/executions/custom?projectKey=CX&autoCreateTestCases=false' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMzBlYjdmOC00NjAwLTMzZTctODNkNi02ODJiYTk3MDM0NTUiLCJjb250ZXh0Ijp7ImJhc2VVcmwiOiJodHRwczpcL1wvcmViZWxiYW5rLmF0bGFzc2lhbi5uZXQiLCJ1c2VyIjp7ImFjY291bnRJZCI6IjVkNWQ1ODQ1YzYzMThhMGQ2Y2Q2YTY1YyJ9fSwiaXNzIjoiY29tLmthbm9haC50ZXN0LW1hbmFnZXIiLCJleHAiOjE2MzQ3NTQ0OTUsImlhdCI6MTYwMzIxODQ5NX0.CIwNAi8GH-LXMOGgRgoWpiTjnpShLXQwokE5NV84Ao8' \
--form 'file=@/Users/bruno/_github-personal/kafka-example/kafka-producer-example/output_results.zip'