package br.com.brunogalvaods.kafka.consumer;

import br.com.brunogalvaods.commons.SpringCommonApp;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@EnableCaching
@SpringBootApplication(scanBasePackages = {"br.com.brunogalvaods.*"})
public class KafkaApplicationConsumer extends SpringCommonApp {

  /**
   * Starts Spring Boot application.
   *
   * @param args Application parameters.
   */
  public static void main(final String[] args) {
    SpringCommonApp.run(KafkaApplicationConsumer.class, args);
  }
}
