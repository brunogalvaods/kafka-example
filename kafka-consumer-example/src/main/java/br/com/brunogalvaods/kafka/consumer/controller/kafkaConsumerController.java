package br.com.brunogalvaods.kafka.consumer.controller;

import br.com.brunogalvaods.commons.exceptions.BusinessException;
import br.com.brunogalvaods.commons.exceptions.IntegrationException;
import br.com.brunogalvaods.commons.model.KafkaModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class kafkaConsumerController {

  private static final Logger LOGGER = LoggerFactory.getLogger(kafkaConsumerController.class);

  @Autowired ObjectMapper objectMapper;

  @Autowired
  @Qualifier("kafkaTemplateTech")
  private KafkaTemplate<String, String> kafkaTemplate;

  @Value("${spring.kafka.consumer.group-id}")
  private String groupId;

  @Value("${kafka.web.topic.name}")
  private String topicName;

  @KafkaListener(topics = "kafkaExampleTopic3", containerFactory = "kafkaListenerContainerFactoryTech")
  public void getMessage(
      ConsumerRecord<String, String> consumerRecord, Acknowledgment acknowledgment)
      throws IOException {

    final String message =
        consumerRecord.value().replace("\"{", "{").replace("}\"", "}").replace("\\", "");
    final KafkaModel model = objectMapper.readValue(message, KafkaModel.class);

    LOGGER.info("thread id: {}", Thread.currentThread().getId());
    LOGGER.info(
        "Consumer kafkaExampleTopic3 - firstName: {}, lastName: {}, personType: {}",
        model.getFirstName(),
        model.getLastName(),
        model.getPersonType());

    if (model.getLastName().contains("integration")) {
      LOGGER.info("IntegrationException message {} ", model);

      final Message<String> messageRetry =
          MessageBuilder.withPayload(consumerRecord.value())
              .setHeader(KafkaHeaders.TOPIC, groupId.concat(".").concat(topicName).concat(".RETRY"))
              .build();

      // Send to retry topic.
      kafkaTemplate.send(messageRetry);
    } else if (model.getLastName().contains("business")) {
      // Manual commit.
      acknowledgment.acknowledge();

      LOGGER.info("BusinessException message {} ", model);
      throw new BusinessException();
    }

    // Manual commit.
    acknowledgment.acknowledge();
  }

  @KafkaListener(
      topics = "KafkaExampleGroupId.kafkaExampleTopic3.RETRY",
      containerFactory = "kafkaListenerContainerFactoryTechRetry")
  public void getMessageRetry(
      ConsumerRecord<String, String> consumerRecord, Acknowledgment acknowledgment)
      throws IOException {
    final String message =
        consumerRecord.value().replace("\"{", "{").replace("}\"", "}").replace("\\", "");
    final KafkaModel model = objectMapper.readValue(message, KafkaModel.class);

    LOGGER.info("thread id: {}", Thread.currentThread().getId());
    LOGGER.info(
        "Consumer kafkaExampleTopic3.RETRY - firstName: {}, lastName: {}, personType: {}",
        model.getFirstName(),
        model.getLastName(),
        model.getPersonType());

    if (model.getLastName().contains("integration")) {
      LOGGER.info("IntegrationException message {} ", model);
      throw new IntegrationException();
    }

    // Manual commit.
    acknowledgment.acknowledge();
  }
}
